# No 1
Monitoring resouce dari Komputer
- RAM
untuk memonitoring RAM pada komputer  kita bisa menggunakan command free untuk melihat total, terpakai, dan memori yang tersisa. Bisa di lihat pada gambar di bawah ini:
   
    ![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-prak.so/-/raw/main/Dokumentasi/Screenshot__145_.png)


    ketika hanya mengetikan command free, yang akan tampil adalah penggunaan RAM berdasarkan satuan kilobyte. bisa diubah tampilannya ke megabyte dengan mengetikan command free --mega atau free --giga. Bisa di lihat pada gambar di bawah ini:
    ![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-prak.so/-/raw/main/Dokumentasi/Screenshot__146_.png)


- CPU
untuk melihat penggunaan CPU pada komputer, kita bisa menggunakan command ps atau top. Bisa di lihat pada gambar di bawah ini:
    ![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-prak.so/-/raw/main/Dokumentasi/Screenshot__147_.png)


- Hardisk
untuk melihat penggunaan Hardisk pada komputer, kita bisa menggunakan command df. Bisa di lihat pada gambar di bawah ini:
    ![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-prak.so/-/raw/main/Dokumentasi/Screenshot__148_.png)

# No 2
Manajemen Program
- Mampu memonitor program yang sedang berjalan
untuk memonitor program yang sedang berjalan pada komputer kita dapat melihatnya menggunakan command ps atau top. Bisa di lihat pada gambar di bawah ini:
    ![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-prak.so/-/raw/main/Dokumentasi/Screenshot__149_.png)

- Mampu menghentikan program yang sedang berjalan
untuk memberhentikan programnya, kita perlu menggunakan command kill <pid>  Bisa di lihat pada gambar di bawah ini:
    ![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-prak.so/-/raw/main/Dokumentasi/Screenshot__150_.png)

    karena pemberhentian program hanya bisa dilakukan oleh pemilik server ssh yang saya gunakan, maka hasilnya akan selalu failed: operation not premitted.
- Otomasi Perintah dengan Shell Script
pertama, kita harus membuat file <nama.file>.sh. 
    ![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-prak.so/-/raw/main/Dokumentasi/Screenshot__151_.png)

    selanjutnya buka file tersebut di vim dengan cara vim <nama.file>.sh.
    ![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-prak.so/-/raw/main/Dokumentasi/Screenshot__153_.png)

    lalu tuliskan #!bin/bash/  keterangan dan $1(parameter)
    ![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-prak.so/-/raw/main/Dokumentasi/Screenshot__155_.png)


    selanjutnya untuk menjalankan shell scriptnya ketikkan ./<nama.file>.sh
    ![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-prak.so/-/raw/main/Dokumentasi/Screenshot__154_.png)

- Penjadwalan eksekusi program dengan cron
untuk menjadwalkan eksekusi program kita bisa menggunakan command crontab -e Bisa di lihat pada gambar di bawah ini:
    ![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-prak.so/-/raw/main/Dokumentasi/Screenshot_2023-05-11_104037.png)

    selanjutnya akan keluar tampilan seperti ini:
    ![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-prak.so/-/raw/main/Dokumentasi/Screenshot__158_.png)
    ![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-prak.so/-/raw/main/Dokumentasi/Screenshot__160_.png)
    disini stadion log dan toilet log sudah tampil pada folder.

# No 3
Manajemen network
- Mengakses sistem operasi pada jaringan menggunakan SSH
    untuk mengakses sistem operasi pada ssh, kita bisa login manual pada ssh dan mengetikan nama server juga passwordnya Bisa di lihat pada gambar di bawah ini:

    ![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-prak.so/-/raw/main/Dokumentasi/Screenshot__161_.png)

- Mampu memonitor program yang menggunakan network
    untuk memonitor program menggunakan network kita bisa menggunakan command netstat -tulpn (untuk mengeluarkan beberapa program yang menggunakan network) dan netstat -tulpn|grep LISTEN (untuk mengeluarkan beberapa program yang menggunakan network pada state LISTEN) hasilnya bisa dilihat pada gambar di bawah ini:

    ![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-prak.so/-/raw/main/Dokumentasi/Screenshot__162_.png)

- Mampu mengoperasikan HTTP client
untuk melihat operasi pada http client kita bisa menggunakan command curl bisa dilihat di bawah ini: 
    ![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-prak.so/-/raw/main/Dokumentasi/Screenshot__163_.png)
    selanjutnya untuk mendownload gambar dan sebagainya bisa menggunakan wget<link> karna disini saya menggunakan website instagram jadi tampilan yang keluar adalah seperti ini
    ![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-prak.so/-/raw/main/Dokumentasi/Screenshot__164_.png)

# No 4
Manajemen file dan folder
- Navigasi, terdapat 3 command yaitu cd untuk masuk kepada direktori, ls untuk melihat isi direktori, dan pwd untuk melihat keberadaan direktori kita sekarang. Bisa dilihat pada gambar di bawah ini untuk prosesnya:
    ![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-prak.so/-/raw/main/Dokumentasi/Screenshot__165_.png)

- CRUD file dan folder untuk editor kita bisa menggunakan (mkdir) untuk membuat folder lalu (touch) untuk melihat apa yang sudah di tambahkan (ls) untuk update atau mengganti nama file kita gunakan command (mv)untuk membuat file,(rmdir) menghapus folder, (rm) untuk menghapus file bisa dilihat gambar dibawah ini untuk implementasinya:
    ![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-prak.so/-/raw/main/Dokumentasi/Screenshot__166_.png)

    untuk otomasi kita bisa gunakan shell script langkah-langkahnya sama seperti otomasi perintah pada soal no 2, berikut dokumentasinya:
    ![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-prak.so/-/raw/main/Dokumentasi/Screenshot__168_.png)
    berikut tampilan cat otomasi.sh :
    ![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-prak.so/-/raw/main/Dokumentasi/Screenshot__169_.png)

- Manajemen ownership dan akses file dan folder
    untuk memenejemen ownership dan akses file dan forlder kita bisa melihat nya terlebih dahulu menggunakan command ls -l
 ![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-prak.so/-/raw/main/Dokumentasi/Screenshot__170_.png)
 
    menggantinya menggunakan chmod (jumlah angka ganti akses owner) nama file. nilai r(read ) adalah 4 nilai w (write) adalah 2 dan x (execute) adalah 1 jadi tinggal di ganti sesuai kebutuhan
    
    hasil mengganti ownership dan akses file
    ![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-prak.so/-/raw/main/Dokumentasi/Screenshot__171_.png)

- Pencarian file dan folder
    memakai command find untuk mencari folder dan file disini saya mencari folder SO yang telah saya buat, implementasinya seperti dokumentasi di bawah ini:
    ![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-prak.so/-/raw/main/Dokumentasi/Screenshot_2023-05-11_115156.png)

- Kompresi data
    untuk mengkompres data bisa menggunakan command tar -cf implementasinya seperti di bawah ini:
    ![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-prak.so/-/raw/main/Dokumentasi/Screenshot_2023-05-11_115447.png)
    setelah dirubah formatnya file nya akan langsung muncul .tar di belakangnya.

# No 5
- Mampu membuat program berbasis CLI menggunakan shell script
Tampilan VIM
![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-prak.so/-/raw/main/Dokumentasi/Screenshot__155_.png)
Tampilan Server
    ![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-prak.so/-/raw/main/Dokumentasi/Screenshot__154_.png)

# No 6
Mendemonstrasikan program CLI yang dibuat kepada publik dalam bentuk Youtube
https://youtu.be/MpLH5lo50EU

# No 7
- Maze Challange
![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-prak.so/-/raw/main/Dokumentasi/Screenshot__172_.png)

    ![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-prak.so/-/raw/main/Dokumentasi/Screenshot__173_.png)

# No 8
- Inisitif 
Saya menambahkan background untuk tampilan maze games saya
![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-prak.so/-/raw/main/Dokumentasi/Screenshot__174_.png)

    Berikut hasil tampilan maze game saya
![Dokumentasi](https://gitlab.com/desainmelia/1217050034-desi-ainul-amelia-prak.so/-/raw/main/Dokumentasi/Screenshot__175_.png)

